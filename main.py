import os
from dotenv import load_dotenv
from tinkoff.invest import Client, InstrumentIdType, OperationType, MoneyValue, OperationState

load_dotenv()
TOKEN = os.getenv('API_TOKEN')

BUY_OPERATION = OperationType(value=15)
SELL_OPERATION = OperationType(value=22)
EXECUTED_SUCCESSFULLY = OperationState(value=1)
FIGI_INSTRUMENT_ID_TYPE = InstrumentIdType(value=1)


# Function returns asset's ticker by it figi
def get_figi_ticker(figi, token):
    with Client(token) as client:
        instruments = client.instruments
        paper = instruments.get_instrument_by(id_type=FIGI_INSTRUMENT_ID_TYPE, id=figi)
        ticker = paper.instrument.ticker
        return ticker


# Function convert MoneyValue value to float type
def convert_money_value(money_value: MoneyValue) -> float:
    value = float(money_value.units) + float(money_value.nano * 10 ** (-9))
    return value


# Function gets operations history of curtain user
def get_operations_history(token) -> dict:
    with Client(token) as client:
        account_id = client.users.get_accounts().accounts[0].id
        operations = client.operations.get_operations(account_id=account_id).operations[::-1]
        # History dictionary initialization
        operations_with_papers = {}
        for operation in operations:
            # Checks if operation is BUY or SELL and was executed successfully
            if (operation.operation_type is SELL_OPERATION or operation.operation_type is BUY_OPERATION) and operation.state is EXECUTED_SUCCESSFULLY:
                ticker = get_figi_ticker(operation.figi, token)
                if ticker not in operations_with_papers.keys():
                    operations_with_papers[ticker] = []
                operations_with_papers[ticker].append({'operation_type': operation.operation_type, 'quantity': operation.quantity, 'price': convert_money_value(operation.price)})
        return operations_with_papers


# Function calculates actual portfolio based on operations history
def calculate_actual_portfolio(operations_history: dict) -> dict:
    actual_portfolio = {}
    for ticker in operations_history.keys():
        if ticker not in actual_portfolio.keys():
            actual_portfolio[ticker] = []

        for operation in operations_history[ticker]:
            # If operation was to BUY assets - just appends to the list
            if operation['operation_type'] is BUY_OPERATION:
                actual_portfolio[ticker].append({'quantity': operation['quantity'], 'price': operation['price']})

            # If operation was to SELL some assets - removes n first elements of the list until needed quantity not sold
            if operation['operation_type'] is SELL_OPERATION:
                quantity_left = operation['quantity']

                while quantity_left != 0:
                    if actual_portfolio[ticker][0]['quantity'] > quantity_left:
                        actual_portfolio[ticker][0]['quantity'] = actual_portfolio[ticker][0]['quantity'] - quantity_left
                        quantity_left = 0
                    else:
                        quantity_left = quantity_left - actual_portfolio[ticker][0]['quantity']
                        actual_portfolio[ticker].pop(0)
    return actual_portfolio


# Function calculates mean assets prices based on the portfolio
def calculate_mean_assets_prices(portfolio: dict) -> dict:
    mean_assets_prices = {}
    for ticker in portfolio.keys():
        # Checks if portfolio has current ticker
        if len(portfolio[ticker]) != 0:
            sum_quantity = 0
            sum_price = 0
            for value in portfolio[ticker]:
                sum_quantity += value['quantity']
                sum_price += value['quantity'] * value['price']
            mean_assets_prices[ticker] = sum_price / sum_quantity

    return mean_assets_prices


# Getting operations history
print('Collecting operations history..')
operations_history = get_operations_history(TOKEN)

# Calculating actual portfolio
print('Calculating actual portfolio..')
actual_portfolio = calculate_actual_portfolio(operations_history=operations_history)

# Calculating mean assets prices
print('Calculating mean assets prices..')
mean_assets_prices = calculate_mean_assets_prices(actual_portfolio)

# Just making output more beautiful
for ticker in mean_assets_prices.keys():
    print(f"Mean {ticker} asset price in actual portfolio is {mean_assets_prices[ticker]}.")